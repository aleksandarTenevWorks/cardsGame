<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class MyAppController extends Controller
{
    public function homeView(){
        return view('home');
    }
    public function formValues(Request $request)
    {
        $type = $request->type;
        $number = $request->number;
        $cardChoosen = $number.$type;
        $deckCards = [
             'AC', 'AD', 'AH' , 'AS', '2C', '2D', '2H', '2S', '3C', '3D', '3H', '3S', '4C', '4D', '4H', '4S','5C', '5D', '5H', '5S', '6C', '6D', '6H', '6S','7C' , '7D' , '7H' , '7S' , '8C' , '8D' , '8H' , '8S' , '9C' , '9D' , '9H' , '9S' , '10C' , '10D' , '10H' , '10S' , 'JC' , 'JD' , 'JH' , 'JS' , 'QC' , 'QD' , 'QH' , 'QS' , 'KC' , 'KD' , 'KH' , 'KS' 
        ];
        session()->put('input', $cardChoosen);
        session()->put('values' , $deckCards);
        $chance = 1 / count(session('values')) * 100 ;
        session()->put('chance' , round($chance , 2));
        Session::save();
        return view('game');
    }
    public function draw()
    {
        $cardChoosen = session('input');
        $deckCards = Session::get('values');
        $cardRandKey = array_rand($deckCards);
        $cardRand = $deckCards[$cardRandKey];
        if($cardChoosen != $cardRand)
        {
            unset($deckCards[$cardRandKey]);     
            session()->put('values', $deckCards);
            $chance = 1 / count(session('values')) * 100 ;
            session()->put('chance' , round($chance , 2));
            session()->put('cardDrawn' , $cardRand);
            Session::save();
            return view('game');
        }
        if ($cardChoosen == $cardRand)
        {
            session()->put('cardDrawn' , $cardRand);
            Session::save();
            return view('win');
        }
        
    }
    public function newGame(){
        session()->flush();
        session()->save();
        return redirect()->route('home');
    }
    
}
