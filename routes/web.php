<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home')->name('home')->uses('MyAppController@homeView');
Route::post('/formValues')->name('formValues')->uses('MyAppController@formValues');
Route::post('/draw')->name('draw')->uses('MyAppController@draw');
Route::post('/newGame')->name('newGame')->uses('MyAppController@newGame');


