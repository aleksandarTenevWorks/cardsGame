@extends('master')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
                <h1>You choosed:</h1>
                    @foreach(session('values') as $cards)
                        @if(session('input') == $cards)
                            <img src="{{ asset('/images/PNG/' . $cards . '.png') }}" alt="" class="cardImg">
                        @endif
                    @endforeach
            </div>
            <div class="col-md-4 text-center">
                <h3>The probability of drawing your card is:<br></h3>
                 <h1>{{ session('chance') }} %</h1>
            </div>
            <div class="col-md-4 col-md-offset-1 text-center">
                <h1>Card drawn:</h1>
                @if(session()->get("cardDrawn") == null)
                <img src="{{ asset('/images/PNG/greenback.png') }}" alt="" class="cardImg">

                @else
                <img src="{{ asset('/images/PNG/' . session('cardDrawn') . '.png') }}" alt="" class="cardImg">
              @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center ">
                <form action="{{route('draw')}}" method="post" class="Btn">
                    <button type="submit" class="btn btn-outline-success">Draw a card</button>
                        {{csrf_field()}}
                </form>
            </div>
        </div>
    </div>

@endsection
