@extends('master')
@section('content')
    <form action="{{route('formValues')}}" method="post" class="text-center">
        <h2>Choose your card</h2>
        <select name="type" class="custom-select">
            <option value="C">Clubes</option>
            <option value="D">Diamonds</option>
            <option value="H">Hearts</option>
            <option value="S">Spades</option>
        </select>
        <select name="number" class="custom-select">
            <option value="A">Ace</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="J">Jack</option>
            <option value="Q">Queen</option>
            <option value="K">King</option>
        </select>
        <button type="submit" class="btn btn-outline-success">Start Game</button>
        {{ csrf_field() }}
    </form>

@endsection


