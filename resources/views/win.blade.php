
@extends('master')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
                <h1>You choosed:</h1>
                    @foreach(session('values') as $cards)
                        @if(session('input') == $cards)
                            <img src="{{ asset('/images/PNG/' . $cards . '.png') }}" alt="" class="cardImg">
                        @endif
                    @endforeach
            </div>
            <div class="col-md-4 text-center">
                <h1>Nice work <br> </h1>
                <form action="{{route('newGame')}}" method="post" class="Btn">
                    <button type="submit" class="btn btn-outline-success">Start new game</button>                
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="col-md-4 col-md-offset-1 text-center">
                <h1>Card drawn:</h1>
                <img src="{{ asset('/images/PNG/' . session('cardDrawn') . '.png') }}" alt="" class="cardImg">
            </div>
        </div>
    </div>


@endsection